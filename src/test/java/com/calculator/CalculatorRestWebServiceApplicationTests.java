package com.calculator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CalculatorRestWebServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalculatorRestWebServiceApplicationTests {

    @LocalServerPort
    private int port;

    @Test
    public void addTwoDigits_whenTwoArePositive_thenResultHasToBePossitive() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/add/param1/1/param2/3"),
                HttpMethod.GET, entity, BigDecimal.class);

        Assert.assertEquals(new BigDecimal(4), response.getBody());
    }

    @Test
    public void addTwoDigits_whenTwoAreNegative_thenResultHasToBeNegative() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/add/param1/-11/param2/-3"),
                HttpMethod.GET, entity, BigDecimal.class);

        Assert.assertEquals(new BigDecimal(-14), response.getBody());
    }

    @Test
    public void testSub() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/sub/param1/12/param2/8"),
                HttpMethod.GET, entity, BigDecimal.class);
        Assert.assertEquals(new BigDecimal(4), response.getBody());
    }

    @Test
    public void testMul() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/mul/param1/3/param2/7"),
                HttpMethod.GET, entity, BigDecimal.class);
        Assert.assertEquals(new BigDecimal(21), response.getBody());
    }

    @Test
    public void testDiv() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/div/param1/30/param2/6"),
                HttpMethod.GET, entity, BigDecimal.class);
        Assert.assertEquals(new BigDecimal(5), response.getBody());
    }

    @Test
    public void testPow() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<BigDecimal> response = restTemplate.exchange(
                createURLWithPort("/div/param1/3/param2/3"),
                HttpMethod.GET, entity, BigDecimal.class);
        Assert.assertEquals(new BigDecimal(27), response.getBody());
    }


    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
