package com.calculator.service;

import java.math.BigDecimal;

public interface CalculatorService {

	BigDecimal add(final BigDecimal arg1, final BigDecimal arg2);

	BigDecimal sub(final BigDecimal arg1, final BigDecimal arg2);

	BigDecimal mul(final BigDecimal arg1, final BigDecimal arg2);

	BigDecimal div(final BigDecimal arg1, final BigDecimal arg2);

	BigDecimal pow(final BigDecimal arg1, final Integer arg2);
}
