package com.calculator.service.impl;

import com.calculator.service.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CalculatorServiceImpl implements CalculatorService {

	private static final Logger logger = LoggerFactory.getLogger(CalculatorServiceImpl.class);

	public BigDecimal add(BigDecimal arg1, BigDecimal arg2) {
		logger.info("Add two digits: {}, {}", arg1, arg2);
		return arg1.add(arg2);
	}

	public BigDecimal sub(BigDecimal arg1, BigDecimal arg2) {
		logger.debug("Subtraction two digits: {}, {}", arg1, arg2);
		return arg1.subtract(arg2);
	}

	public BigDecimal mul(BigDecimal arg1, BigDecimal arg2) {
		logger.info("Multiply two digits: {}, {}", arg1, arg2);
		return arg1.multiply(arg2);
	}

	public BigDecimal div(BigDecimal arg1, BigDecimal arg2) {
		logger.debug("Divide two digits: {}, {}", arg1, arg2);
		return arg1.divide(arg2);
	}

	@Override
	public BigDecimal pow(BigDecimal arg1, Integer arg2) {
		logger.debug("Power of digit: {}, {}", arg1, arg2);
		return arg1.pow(arg2);
	}
}
