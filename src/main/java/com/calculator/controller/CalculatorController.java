package com.calculator.controller;

import com.calculator.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/")
public class CalculatorController implements CalculatorService {

	@Autowired
	private CalculatorService calculatorService;

	@Override
	@RequestMapping(value = "/add/param1/{param1}/param2/{param2}", method = RequestMethod.GET)
	public BigDecimal add(@PathVariable BigDecimal param1, @PathVariable BigDecimal param2) {
		return calculatorService.add(param1, param2);
	}

	@Override
	@RequestMapping(value = "/sub/param1/{param1}/param2/{param2}", method = RequestMethod.GET)
	public BigDecimal sub(@PathVariable BigDecimal param1, @PathVariable BigDecimal param2) {
		return calculatorService.sub(param1, param2);
	}

	@Override
	@RequestMapping(value = "/mul/param1/{param1}/param2/{param2}", method = RequestMethod.GET)
	public BigDecimal mul(@PathVariable BigDecimal param1, @PathVariable BigDecimal param2) {
		return calculatorService.mul(param1, param2);
	}

	@Override
	@RequestMapping(value = "/div/param1/{param1}/param2/{param2}", method = RequestMethod.GET)
	public BigDecimal div(@PathVariable BigDecimal param1, @PathVariable BigDecimal param2) {
		return calculatorService.div(param1, param2);
	}

	@Override
	@RequestMapping(value = "/pow/param1/{param1}/param2/{param2}", method = RequestMethod.GET)
	public BigDecimal pow(@PathVariable BigDecimal param1, @PathVariable Integer param2) {
		return calculatorService.pow(param1, param2);
	}
}
