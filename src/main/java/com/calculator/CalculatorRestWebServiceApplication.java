package com.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatorRestWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorRestWebServiceApplication.class, args);
	}

}
